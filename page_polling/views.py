# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.db import connection

# Create your views here.

def index(request):
    response = {}

    cursor = connection.cursor()
    query = 'SELECT PB.id_polling, PB.deskripsi, PB.url, P.polling_start, P.polling_end , P.total_responden FROM POLLING P, POLLING_BIASA PB where PB.id_polling = P.id;'
    cursor.execute(query)
    polling = cursor.fetchall()
    response['polling'] = polling

    return render(request, 'polling.html', response)
