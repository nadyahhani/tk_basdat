# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Polling(models.Model):
    id = models.IntegerField(primary_key=True)
    polling_start = models.DateTimeField()
    polling_end = models.DateTimeField()
    total_responden = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'polling'


class PollingBiasa(models.Model):
    id_polling = models.ForeignKey(Polling, models.DO_NOTHING, db_column='id_polling', primary_key=True)
    url = models.CharField(max_length=50)
    deskripsi = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'polling_biasa'