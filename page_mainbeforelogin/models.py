# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from page_berita.models import skema_universitas

class skema_narasumber(models.Model):
    id = models.IntegerField(blank=False, primary_key=True, unique=True)
    nama = models.CharField(blank=False, max_length=50)
    email = models.CharField(blank=False, max_length=50)
    tempat = models.CharField(blank=False, max_length=50)
    tanggal = models.CharField(blank=False, max_length=50)
    no_hp = models.CharField(blank=False, max_length=50)
    jumlah_berita = models.IntegerField(blank=False)
    rerata_kata = models.IntegerField(blank=False)
    id_universitas = models.ForeignKey('page_berita.skema_universitas', on_delete=models.CASCADE)
    class Meta:
        managed = False
        db_table = 'narasumber'

class skema_mahasiswa(models.Model):
    id_narasumber = models.ForeignKey('skema_narasumber', on_delete=models.CASCADE)
    npm = models.CharField(blank=False, max_length=20)
    status = models.CharField(blank=False, max_length=20)

    class Meta:
        managed = False
        db_table = 'mahasiswa'

class skema_dosen(models.Model):
    id_narasumber = models.ForeignKey('skema_narasumber', on_delete=models.CASCADE)
    nik_dosen = models.CharField(blank=False, max_length=20)
    jurusan = models.CharField(blank=False, max_length=20)

    class Meta:
        managed = False
        db_table = 'dosen'

class skema_staf(models.Model):
    id_narasumber = models.ForeignKey('skema_narasumber', on_delete=models.CASCADE)
    nik_staf = models.CharField(blank=False, max_length=20)
    posisi = models.CharField(blank=False, max_length=20)

    class Meta:
        managed = False
        db_table = 'staf'
