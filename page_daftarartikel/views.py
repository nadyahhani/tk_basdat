# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.db import connection

# Create your views here.

def index(request):
    response = {}

    cursor = connection.cursor()

    #narasumber = request.session['id']
    idNarasumber = 2

    query = 'SELECT url FROM BERITA B, NARASUMBER_BERITA NB, NARASUMBER N WHERE NB.id_narasumber = N.id and NB.url_berita = B.url and N.id =' +str(idNarasumber)
    cursor.execute(query)
    berita = cursor.fetchall()

    daftar_berita = []
    for x in berita:
        query = "SELECT * FROM BERITA WHERE url = '" + str(x[0]) + "'"
        cursor.execute(query)
        obj = cursor.fetchone()[0]
        daftar_berita.append(obj)
    
    response['daftar_berita'] = daftar_berita
    print(daftar_berita)
    return render(request, 'daftarartikel.html', response)
