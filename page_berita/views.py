from __future__ import unicode_literals

from django.shortcuts import render
from .models import skema_berita,skema_universitas

# Create your views here.

def index(request):
    response = {}
    berita = skema_berita.objects.all();
    response['berita'] = berita
    return render(request, 'berita.html', response)
