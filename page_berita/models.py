# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class skema_berita(models.Model):
    url = models.CharField(max_length=50, blank=False, primary_key=True, unique=True)
    judul = models.CharField(max_length=100, blank=False)
    topik = models.CharField(max_length=100, blank=False)
    Created_at = models.DateTimeField(auto_now_add=True)
    Updated_at = models.DateTimeField(auto_now=True)
    jumlah_kata = models.IntegerField(blank=False)
    rerata_rating = models.DecimalField(max_digits=2, decimal_places=1)
    id_universitas = models.ForeignKey('skema_universitas', on_delete=models.CASCADE,)

class skema_universitas(models.Model):
    id = models.IntegerField(blank=False, primary_key=True, unique=True)
    jalan = models.CharField(blank=False, max_length=100)
    kelurahan = models.CharField(blank=False, max_length=50)
    provinsi = models.CharField(blank=False, max_length=50)
    kodepos = models.CharField(blank=False, max_length=10)
    website = models.CharField(blank=False, max_length=50)

class skema_riwayat(models.Model):
    url_berita = models.ForeignKey('skema_berita', on_delete=models.CASCADE, blank=False, max_length=50, primary_key=True, unique=True)
    id_riwayat = models.IntegerField(blank=False, primary_key=True, unique=True)
    waktu_revisi = models.DateTimeField(auto_now=True)
    konten = models.TextField(blank=False)
