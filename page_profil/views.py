# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect, reverse
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.db import connection


# Create your views here.

def index(request):
    response = {}
    cursor = connection.cursor()
    cursor.execute('SELECT COUNT(*) FROM NARASUMBER')

    total = cursor.fetchone()[0]

    if (total != 0) :
        #narasumber = request.session['id']
        idNarasumber = 2

        query = 'SELECT nama FROM NARASUMBER WHERE id ='+str(idNarasumber)
        cursor.execute(query)
        nama = cursor.fetchone()[0]
        response['nama'] = nama

        query = 'SELECT username FROM NARASUMBER WHERE id ='+ str(idNarasumber)
        cursor.execute(query)
        username = cursor.fetchone()[0]
        response['username'] = username

        mahaBool = False;
        doseBool = False;

        role = ''
        pilihan_judul = ''
        pilihan_isi = ''

        queryMahasiswa = 'SELECT id_narasumber FROM MAHASISWA WHERE id_narasumber ='+ str(idNarasumber)
        cursor.execute(queryMahasiswa)
        hasil = cursor.fetchone()
        if (hasil is not None) :
            role = 'Mahasiswa'
            query = 'SELECT npm FROM MAHASISWA WHERE id_narasumber ='+ str(idNarasumber)
            cursor.execute(query)
            noIden = cursor.fetchone()[0]

            pilihan_judul = 'Status'
            query = 'SELECT status FROM MAHASISWA WHERE id_narasumber ='+ str(idNarasumber)
            cursor.execute(query)
            pilihan_isi = cursor.fetchone()[0]

            response['noIden'] = noIden
            response['pilihan_judul'] = pilihan_judul
            response['pilihan_isi'] = pilihan_isi
            mahaBool = True


        queryDosen = 'SELECT id_narasumber FROM DOSEN where id_narasumber ='+ str(idNarasumber)
        cursor.execute(queryDosen)
        hasil = cursor.fetchone()
        if (hasil is not None and mahaBool == False) :
            role = 'Dosen'
            query = 'SELECT nik_dosen FROM DOSEN WHERE id_narasumber ='+ str(idNarasumber)
            cursor.execute(query)
            noIden = cursor.fetchone()[0]

            pilihan_judul = 'Jurusan'
            query = 'SELECT jurusan FROM DOSEN WHERE id_narasumber ='+ str(idNarasumber)
            cursor.execute(query)
            pilihan_isi = cursor.fetchone()[0]

            response['noIden'] = noIden
            response['pilihan_judul'] = pilihan_judul
            response['pilihan_isi'] = pilihan_isi
            doseBool = True

        queryStaf = 'SELECT id_narasumber FROM STAF WHERE id_narasumber ='+ str(idNarasumber)
        cursor.execute(queryStaf)
        hasil = cursor.fetchone()
        if (hasil is not None and (mahaBool == False and doseBool == False)) :
            role = 'Staff'
            response['no_text'] = 'NIK'
            query = 'SELECT nik_staf FROM STAF WHERE id_narasumber='+ str(nara)
            cursor.execute(query)
            noIden = cursor.fetchone()[0]

            pilihan_judul = 'Posisi'
            query = 'SELECT posisi FROM STAF WHERE id_narasumber ='+ str(idNarasumber)
            cursor.execute(query)
            pilihan_isi = cursor.fetchone()[0]

            response['noIden'] = noIden
            response['pilihan_judul'] = pilihan_judul
            response['pilihan_isi'] = pilihan_isi

        response['role'] = role


        query = 'SELECT id_universitas FROM narasumber WHERE id ='+ str(idNarasumber)
        cursor.execute(query)
        id_univ = cursor.fetchone()[0]
        response['id_univ'] = id_univ

        query = "SELECT tanggal FROM NARASUMBER WHERE id =" + str(idNarasumber)
        cursor.execute(query)
        bday = cursor.fetchone()[0]
        response['bday'] = bday

        query = 'SELECT tempat FROM narasumber WHERE id ='+ str(idNarasumber)
        cursor.execute(query)
        tempat = cursor.fetchone()[0]
        response['tempat'] = tempat

        query = 'SELECT email FROM narasumber WHERE id ='+ str(idNarasumber)
        cursor.execute(query)
        email = cursor.fetchone()[0]
        response['email'] = email

        query = 'SELECT no_hp FROM narasumber WHERE id ='+ str(idNarasumber)
        cursor.execute(query)
        no_hp = cursor.fetchone()[0]
        response['no_hp'] = no_hp



    return render(request, 'profil.html', response)
